FROM mhart/alpine-node:12 as development
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 7070
CMD [ "node", "server.js" ]

FROM mhart/alpine-node:12 as test
WORKDIR /usr/src/app
COPY --from=development /usr/src/app/ .
CMD [ "npm", "run", "test" ]

FROM mhart/alpine-node:12 as production
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci --only=production
COPY . .
EXPOSE 7070
CMD [ "node", "server.js" ]
